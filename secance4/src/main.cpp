#include<iostream>
#include<vector>

using namespace std;

int main(){
	/*Description : Declaration d'un tableau*/

	vector<int>tab(3);	//créeation  du tableau avec le type  puis le nom avec 3 cases
	tab[0] = 3;
	tab[1] = 4;
	tab[2] = 5;

	tab.push_back(1000);	//Fonction qui permet de rajouter un element entier

	cout<<tab[0]<<" "<<tab[1]<<" "<<tab[2]<<" "<<tab[3]<<endl;

	cout<<tab.size()<<endl;		//Voir le taille de valeur

	tab[3]=101;			// On peux commencer à ajouter des valeurs jusqu'à 1000
	cout<<tab[3]<<endl;
	return 0;
}
