#include <iostream>
#include <vector>
#include <string>
#include <fstream>

using namespace std;


class etudiant
{
public :
	string nom;			//On peut directement créer la classe
	int note;			//Pour des petits classes
};



int main(){

	/* Phase 1 : Cretaion et remplissage des tableau*/
	vector <etudiant> epsi;
	string tmp_name;
	int tmp_note;

	ofstream f_notes;
	f_notes.open("notes.txt");

	for(int i=0; i<3; i++){
		cout<<"Nom de l'etudiant : "<<endl;
		cin>>tmp_name;
		cout<<"Note de l'etudiant : "<<endl;
		cin>>tmp_note;
		epsi.push_back(etudiant());		//creer un etudiant
		epsi[i].nom = tmp_name;
		epsi[i].note = tmp_note;

		// On ecrit le nom et la note dans le fichier
		f_notes<<epsi[i].nom<<"		"<<epsi[i].note<<endl;
	}
	f_notes.close();

	cout<<endl<<"La liste  des etudiants avec leur notes"<<endl; 
	for(int i=0; i<epsi.size(); i++){
		cout<<epsi[i].nom<<"		"<<epsi[i].note<<endl;
	}

	/* Phase 2 : Afficher la note en fonction du nom */
	cout<<"Entez le nom a recherchez"<<endl;
	for(int i=0; i<epsi.size(); i++){
		if(tmp_name == epsi[i].nom){
		cout<<"Note etudiant "<<epsi[i].nom<<" est "<<epsi[i].note<<endl;
		//break;
		}
	}

	/* Gerer le persistence des notes 
	ofstream fichier_notes;
	fichier_notes.open("file.txt");
	fichier_notes.close();
	*/

	return 0;
}
