/*!\file main.cpp
* \brief Controle des nombres réels
* \author Ganesh
* \version 1.0
* \date 17 octobre 2018
*/


#include <iostream>
#include <string>
#include <vector>
#include <fstream>

using namespace std;

class reel		//Création de la classe reel
{			//Avec un float nbr
public :
	float nbr;
};

int main()
{
int menu=0;
while(menu==0)
{
	int value;

	cout<<"|----| Controle : Gèrer une liste de nombre réels |----|"<<endl;

	cout<<"1 Ajouter un réel"<<endl;
	cout<<"2 Afficher la liste des réel"<<endl;
	cout<<"3 Supprimer le premier réel ayant une valeur"<<endl;
	cout<<"4 Supprimer tous les réels ayant une valeur donnée"<<endl;
	cout<<"5 Quitter"<<endl;
	cin>>value;

/*!\brief création du tableau*/
	vector <reel> nombre;		//création d'un tableau
	float tmp_nbr;


	switch(value)
	{
/*!\brief ajout d'un reel*/
		case 1 :{cout<<"| Ajouter un réel |"<<endl;

			 ofstream f_notes;
			 f_notes.open("nombreReel.txt");
			 cin>>tmp_nbr;
			 f_notes<<tmp_nbr<<endl;
			 f_notes.close();
			 break;}
/*!\brief afficher liste*/

		case 2 : {cout<<"| Afficher la liste des réel |"<<endl;
			 for(int i=0; i<nombre.size(); i++){
				nombre[i].nbr = tmp_nbr;
				cout<<nombre[i].nbr<<endl;
			 }
			 break;}

/* \brief supprimer des value
* param int deltevalue : valeur de l'utilistateur à supprimer
*/
		case 3 : int deltevalue
			 {cout<<"| Supprimer le premier réel ayant une valeur |"<<endl;
			 cin>>deltevalue;
			 if(deltevalue==tmp_nbr){
				nombre.erase(nombre.begin()+deltevalue)
			 break;}

		case 4 : {cout<<"| Supprimer tous les réels ayant une valeur donnée |"<<endl;

			 break;}

		case 5 : break;

		default : cout<<"Error"<<endl;
			  break;
	}
}
	return 0;
}
