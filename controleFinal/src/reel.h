/*!file reel.h
* \breif Class reel
* \author Ganesh
* \version 1.0
* \date 17 octobre 2018
*/

#pragma once 
#include <iostream>

/*!\ class reel
* \brief class avec une variable pour les reel
*/

class reel
{
public :
/*!\ brief fonction reel
* \param float nbr : nombre reel
*/

	float nbr;
};
