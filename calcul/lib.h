/*!\file lib.h
*\brief description
*\author Ganesh
*\version 1.0
*\date 26 septembre 2018
*
* Class calcul: Somme-Multi
*
*/

#pragma once 

/*!\class calcul
*
*  \brief classe représentant les calculs de somme et de multiplication 
*
*/
class calcul
{
public :
	/*!\brief fonction somme
	*  \param int nb1 : varaible 1 pour somme
	*  \param int nb2 : variable 2 pour somme
	*/
	int somme(int nb1, int nb2);

	/*!\brief fonction multiplication
	*  \param int nb3 : variable 3 pour multiplication
	*  \param int nb4 : variable 4 pour multiplication 
	*/
	int multi(int nb3, int nb4);
};
