#include "triangle.h"
#include <iostream>

/*!\brief fonction perimetre
 * \calcul du perimetre: cote + cote + cote
*/
float triangle::perimetre(float pctp , float dctp , float tctp){
	float tperi= pctp + dctp + tctp;
	return tperi;
}

/*!\brief fonction aire
 * \calcul de l'aire : (bases * hauteur)/2
*/
float triangle::aire(float pgcta, float hta){
	float taire= (pgcta * hta)/2;
	return taire;
} 
