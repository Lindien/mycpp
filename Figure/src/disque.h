/*!\file disque.h
* \brief Class Disque
* \author Ganesh
* \version 1.0
* \date 03 octobre 2018
*/

#pragma once 
#include <iostream>
#include "figure.h"

/*!\ class disque 
* \brief class avec calcul de l'aire et perimetre d'un disque
*/
class disque : public figure
{
public :
/*!\brief fonction perimetre de disque
* \param float rp : Rayon Perimetre
*/
	float perimetre(float rp);

/*!\brief fonction aire de dsique
* \param float ra : Rayon Aire
*/
	float aire(float ra);
};
