/*!\file figure.h
* \brief description
* \author Ganesh
* \version 1.0
* \date 03 octobre 2018
*/

#pragma once
#include <iostream>

class figure
{
public : 
/*\brief declaration des fonctions virtual
* \param virtual function pour le perimetre
*/
	virtual float perimetre();

/*\brief declaration des fonctions virtual
* \param virtual function pour l'aire
*/
	virtual float aire();


};
