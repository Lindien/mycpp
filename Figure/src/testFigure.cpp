#include <iostream>
#include <stdlib.h>
#include <string>
#include "figure.h"
#include "triangle.h"
#include "rectangle.h"
#include "disque.h"

using namespace std;

int main()
{
	triangle* tri;
	rectangle* rec;
	disque* dis;

	int value;

	cout<<"|----| Programme calcul de perimetre et aire d'une figure |----|"<<endl<<endl<<endl;

	cout<<"Choisisez votre calcul :"<<endl;
	cout<<" 1 Aire d'un triangle"<<endl;
	cout<<" 2 Aire d'un rectangle"<<endl;
	cout<<" 3 Aire d'un disque"<<endl;
	cout<<" 4 Perimetre d'un triangle"<<endl;
	cout<<" 5 Perimetre d'un rectangle"<<endl;
	cout<<" 6 Perimetre d'un disque"<<endl;
	cout<<" 7 Quitter"<<endl;

	cin>>value;
	switch(value)
	{
		case 1 : float pgcta, hta;
			 cout<<"| Aire d'un triangle |"<<endl;
			 cout<<"Plus grand cote du triangle"<<endl;
			 cin>>pgcta;
			 cout<<"Hauteur du triangle"<<endl;
			 cin>>hta;
			 cout<<"Voici l'aire du triangle : "<< tri->aire(pgcta, hta)<<endl;
			 break;


		case 2 : float La, la;
			 cout<<"| Aire d'un rectangle |"<<endl;
			 cout<<"Largeur du rectangle"<<endl;
			 cin>>La;
 			 cout<<"Longeur du rectangle"<<endl;
			 cin>>la;
			 cout<<"Voici l'aire du rectangle : "<< rec->aire(La, la);
			 break;


		case 3 : float ra;
			 cout<<"| Aire d'un disque |"<<endl;
			 cout<<"Rayon du disque : "<<endl;
			 cin>>ra;
			 cout<<"Voici l'aire du disque : "<< dis->aire(ra);
			 break;


		case 4 : float pctp, dctp, tctp;
			 cout<<"| Perimetre d'un tirangle |"<<endl;
			 cout<<"Donnez les valeurs des trois cotes du triangle : "<<endl;
			 cin>>pctp;
			 cin>>dctp;
			 cin>>tctp;
			 cout<<"Voici le perimetre du triangle : "<< tri->perimetre(pctp, dctp, tctp)<<endl;
			 break;


		case 5 : float Lp, lp;
			 cout<<"| Perimetre d'un rectangle |"<<endl;
			 cout<<"Largeur du rectangle"<<endl;
			 cin>>Lp;
 			 cout<<"Longeur du rectangle"<<endl;
			 cin>>lp;
			 cout<<"Voici le permietre du rectangle : "<< rec->perimetre(Lp, lp);
			 break;


		case 6 : float rp;
			 cout<<"| Perimetre d'un disque |"<<endl;
			 cout<<"Rayon du disque : "<<endl;
			 cin>>rp;
			 cout<<"Voici le perimetre du disque : "<< dis->perimetre(rp);
			 break;

		case 7 : break;

		default : cout<<"Error"<<endl;
			 break;
	}
	return 0;
}
