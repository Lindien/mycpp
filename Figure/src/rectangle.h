/*!\file rectangle.h
* \breif Class Rectangle 
* \author Ganesh
* \version 1.0
* \date 03 octobre 2018
*/

#pragma once
#include <iostream>
#include "figure.h"


/*!\ class rectangle
* \brief class avec calcul de l'aire et perimetre d'un rectangle
*/
class rectangle : public figure
{
public :
/*!\brief fonction perimetre de rectangle
* \ param float Lp : Largeur perimetre
* \ param float lp : longeur perimetre
*/
	float perimetre(float Lp, float lp);

/*!\brief fonction aire de rectangle
* \param float La : Largeur aire
* \param float la : longeur aire
*/
	float aire(float La, float la);
};
