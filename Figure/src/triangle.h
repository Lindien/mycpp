/*! \file triangle.h
*  \brief Classe Triangle
*  \author Ganesh
*  \version 1.0
*  \date 03 octobre 2018
*/

#pragma once
#include <iostream>
#include "figure.h"


/*!\ class triangle
*  \brief class triangle pour calcul de l'aire et perimetre d'un triangle
*/

class triangle : public figure
{
public :
/*!\brief fonction perimetre de triangle
* \param float pctp : premier cote du triangle perimetre
* \param float dctp : deuxieme cote du triangle perimetre
* \param float tctp : troisieme cote du triangle perimetre
*/
	float perimetre(float pctp, float dctp, float tctp);

/*!\brief fonction aire detriangle
* \param float pgcta : plus grand cote du triangle aire
* \param float hta : hauteur du triangle aire
*/
	float aire(float pgcta, float hta);
};
